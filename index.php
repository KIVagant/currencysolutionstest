<?php
/**
 * @author Eugene Glotov <kivagant@gmail.com>
 * @package test
 *
 * All parameters is optional
 *
 * Usage examples:
 *  php index.php
 *  php index.php statistics:payments
 *  php index.php statistics:payments resources/statement.csv
 *  php index.php statistics:payments resources/statement.csv json all
 *  php index.php statistics:payments resources/statement.csv json payments_reference
 *  php index.php statistics:payments resources/statement.csv plain_text payments_reference
 *  php index.php statistics:payments resources/statement.csv plain_text all sum_by_currency
 *  php index.php statistics:payments resources/statement.csv plain_text payments_reference sum_by_currency
 */

require __DIR__ . '/vendor/autoload.php';

$application = new \KIVagant\StatementParser\StatementParser();
$application->registerServices();
$application->registerCommands();
$application->run();
