# Statistics parser (Currency solutions test)

## Usage:
``` php index.php statistics:payments [filepath] [format] [filter] [indicator]```

or

``` payments.sh [filepath] [format] [filter] [indicator]```

See app/config/statistics.php for getting available attributes.

## Examples:

 * ```payments.sh```
 * ```payments.sh resources/statement.csv```
 * ```payments.sh resources/statement.csv json all```
 * ```payments.sh resources/statement.csv json payments_reference```
 * ```payments.sh resources/statement.csv plain_text payments_reference```
 * ```payments.sh resources/statement.csv plain_text all sum_by_currency```
 * ```payments.sh resources/statement.csv json cursol sum_by_currency```