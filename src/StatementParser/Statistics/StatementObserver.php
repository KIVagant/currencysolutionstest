<?php
namespace KIVagant\StatementParser\Statistics;
use KIVagant\StatementParser\Statistics\Filters\FilterInterface;

/**
 * @author Eugene Glotov <kivagant@gmail.com>
 * @package test
 */
class StatementObserver
{
    protected $filter;
    protected $rows;

    public function __construct(&$rows, FilterInterface $filter)
    {
        $this->rows = &$rows;
        $this->filter = $filter;
    }

    public function observe(array $row)
    {
        if ($this->filter->filter($row)) {
            $this->rows[] = array(
                'credit' => $row[7],
                'debit' => $row[8],
                'currency' => $row[9],
            );
        }
    }
}