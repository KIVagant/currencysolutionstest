<?php
namespace KIVagant\StatementParser\Statistics\Filters;

/**
 * @author Eugene Glotov <kivagant@gmail.com>
 * @package test
 */
class PaymentReferenceFilter implements FilterInterface
{
    public function filter($row)
    {
        return preg_match('/PAY[0-9]{6}[a-z]{2}/i', $row[1]);
    }
}