<?php
namespace KIVagant\StatementParser\Statistics\Filters;

/**
 * @author Eugene Glotov <kivagant@gmail.com>
 * @package test
 */
class CursolFilter implements FilterInterface
{
    public function filter($row)
    {
        return ('CURSOL' === $row[3]);
    }
}