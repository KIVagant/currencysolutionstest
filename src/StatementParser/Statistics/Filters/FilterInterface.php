<?php
/**
 * @author Eugene Glotov <kivagant@gmail.com>
 * @package test
 */
namespace KIVagant\StatementParser\Statistics\Filters;

interface FilterInterface
{
    public function filter($row);
}