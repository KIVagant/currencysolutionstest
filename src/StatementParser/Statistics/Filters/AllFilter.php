<?php
/**
 * @author Eugene Glotov <kivagant@gmail.com>
 * @package test
 */
namespace KIVagant\StatementParser\Statistics\Filters;

class AllFilter implements FilterInterface
{
    public function filter($row)
    {

        return (bool) \DateTime::createFromFormat('d/m/Y', $row[0]);
    }
}