<?php
/**
 * Created by PhpStorm.
 * User: kivagant
 * Date: 8/6/15
 * Time: 02:52
 */

namespace KIVagant\StatementParser\Statistics\Indicators;

class SumByCurrencyIndicator implements IndicatorsInterface
{
    protected $rows = [];
    public function __construct($rows)
    {
        $this->rows = $rows;
    }
    public function calc()
    {
        $currencies = [];
        foreach ($this->rows as $data) {
            if (!array_key_exists($data['currency'], $currencies)) {
                $currencies[$data['currency']] = 0;
            }
            $currencies[$data['currency']] += (float) $data['debit'] - (float) $data['credit'];
        }

        return $currencies;
    }
}