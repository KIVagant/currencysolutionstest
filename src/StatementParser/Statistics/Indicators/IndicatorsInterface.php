<?php
/**
 * Created by PhpStorm.
 * User: kivagant
 * Date: 8/6/15
 * Time: 02:52
 */

namespace KIVagant\StatementParser\Statistics\Indicators;

interface IndicatorsInterface
{
    public function __construct($rows);
    public function calc();
}