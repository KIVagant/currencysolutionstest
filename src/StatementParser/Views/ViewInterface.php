<?php
namespace KIVagant\StatementParser\Views;

/**
 * @author Eugene Glotov <kivagant@gmail.com>
 * @package test
 */

interface ViewInterface
{
    public function render(array $data);
}