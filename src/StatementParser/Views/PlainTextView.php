<?php
namespace KIVagant\StatementParser\Views;

/**
 * @author Eugene Glotov <kivagant@gmail.com>
 * @package test
 */

class PlainTextView implements ViewInterface
{
    public function render(array $data)
    {
        $result = 'Total:' . PHP_EOL;
        ksort($data);

        foreach ($data as $currency => $sum) {
            $result .= $currency . ' ' . round($sum, 2) . PHP_EOL;
        }

        return $result;
    }
}