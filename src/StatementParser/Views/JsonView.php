<?php
namespace KIVagant\StatementParser\Views;

/**
 * @author Eugene Glotov <kivagant@gmail.com>
 * @package test
 */

class JsonView implements ViewInterface
{
    public function render(array $data)
    {
        ksort($data);

        return json_encode($data);
    }
}