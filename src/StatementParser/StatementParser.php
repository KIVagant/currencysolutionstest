<?php
namespace KIVagant\StatementParser;

use Symfony\Component\Console\Application;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * @author Eugene Glotov <kivagant@gmail.com>
 * @package test
 */
class StatementParser extends Application
{
    /**
     * @var ContainerBuilder
     */
    protected $container;

    /**
     * We can use symfony PhpFileLoader, of course, but I don't like symfony/config bundle.
     * So, let's make yet another bicycle! =)
     *
     * @throws \Exception
     */
    public function registerServices()
    {
        $this->container = new ContainerBuilder();
        $this->container
            ->register('config', 'Noodlehaus\Config')
            ->addArgument('app/config/');
        $config = $this->container->get('config');
        foreach ($config['services'] as $service_name => $params) {
            $service = $this->container->register($service_name, $params['class']);
            foreach ($params['arguments'] as $argument) {
                if (substr($argument, 0, 1) === '@') {
                    $service->addArgument(new Reference(str_replace('@' , '', $argument)));
                } else {
                    $service->addArgument($argument);
                }
            }
        }
    }

    /**
     * @throws \Exception
     */
    public function registerCommands()
    {
        $config = $this->container->get('config');
        foreach ($config['commands'] as $command) {
            $command = $this->container->get($command);
            $this->add($command);
        }
        $this->setDefaultCommand($config->get('default_command'));
    }
}

