<?php

// For OS X: http://php.net/manual/en/filesystem.configuration.php#ini.auto-detect-line-endings
if (! ini_get('auto_detect_line_endings')) {
    ini_set('auto_detect_line_endings', '1');
}