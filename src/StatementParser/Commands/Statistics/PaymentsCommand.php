<?php
namespace KIVagant\StatementParser\Commands\Statistics;

use Goodby\CSV\Import\Standard\Interpreter;
use Goodby\CSV\Import\Standard\Lexer;
use KIVagant\StatementParser\Exceptions\UnknownFilterException;
use KIVagant\StatementParser\Exceptions\UnknownIndicatorException;
use KIVagant\StatementParser\Exceptions\UnknownViewException;
use KIVagant\StatementParser\Statistics\StatementObserver;
use Noodlehaus\ConfigInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @author Eugene Glotov <kivagant@gmail.com>
 * @package test
 */
class PaymentsCommand extends Command
{
    /**
     * @var ConfigInterface
     */
    protected $config;
    public function __construct($name = null, ConfigInterface $config)
    {
        $this->config = $config;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('statistics:payments')
            ->setDescription('Will parse file by path and output ')
            ->addArgument(
                'file',
                InputArgument::OPTIONAL,
                'Path to file that will be parsed',
                $this->config->get('resources.default')
            )
            ->addArgument(
                'view',
                InputArgument::OPTIONAL,
                'Output format: '
                . var_export($this->config->get('statistics.payments.views.available'), true),
                $this->config->get('statistics.payments.views.default')
            )
            ->addArgument(
                'filter',
                InputArgument::OPTIONAL,
                'Filter class that will be used for rows: '
                    . var_export($this->config->get('statistics.payments.filters.available'), true),
                $this->config->get('statistics.payments.filters.default')
            )
            ->addArgument(
                'indicator',
                InputArgument::OPTIONAL,
                'Statistic indicator class that will be used for result calculation: '
                    . var_export($this->config->get('statistics.payments.indicators.available'), true),
                $this->config->get('statistics.payments.indicators.default')
            );

    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $rows = [];
        $path = $input->getArgument('file');

        $filter_class = $this->checkFilter($input);
        $indicator_class = $this->checkIndicator($input);
        $view_class = $this->checkView($input);

        $filter = new $filter_class();
        $observer = new StatementObserver($rows, $filter);

        $interpreter = new Interpreter();
        $interpreter->addObserver([$observer, 'observe']);

        $lexer = new Lexer();
        $lexer->parse($path, $interpreter);

        $statistics = new $indicator_class($rows);
        $output_data = $statistics->calc();

        $view = new $view_class();
        $output->writeln($view->render($output_data));
    }

    /**
     * @param InputInterface $input
     * @return mixed
     * @throws UnknownFilterException
     */
    protected function checkFilter(InputInterface $input)
    {
        $filter_class = $input->getArgument('filter');
        $available_filters = $this->config->get('statistics.payments.filters.available');
        if (!array_key_exists($filter_class, $available_filters)) {
            throw new UnknownFilterException('Unknown filter ' . $filter_class);
        }

        return $this->config->get('statistics.payments.filters.available.' . $filter_class);
    }

    /**
     * @param InputInterface $input
     * @return mixed
     * @throws UnknownIndicatorException
     */
    protected function checkIndicator(InputInterface $input)
    {
        $indicator_class = $input->getArgument('indicator');
        $available_indicators = $this->config->get('statistics.payments.indicators.available');
        if (!array_key_exists($indicator_class, $available_indicators)) {
            throw new UnknownIndicatorException('Unknown indicator ' . $indicator_class);
        }

        return $this->config->get('statistics.payments.indicators.available.' . $indicator_class);
    }

    /**
     * @param InputInterface $input
     * @return mixed
     * @throws UnknownViewException
     */
    protected function checkView(InputInterface $input)
    {
        $view_class = $input->getArgument('view');
        $available_views = $this->config->get('statistics.payments.views.available');
        if (!array_key_exists($view_class, $available_views)) {
            throw new UnknownViewException('Unknown view ' . $view_class);
        }

        return $this->config->get('statistics.payments.views.available.' . $view_class);
    }
}