#!/usr/bin/env bash
echo "############################################"
echo "     Welcome to test statistic parser!"
echo "############################################"
filepath=$1
if [ "$filepath" == "" ]; then
    echo "Filepath is empty. Default will be used."
fi

format=$2
if [ "$format" == "" ]; then
    echo "Format is empty. Default will be used."
fi

filter=$3
if [ "$filter" == "" ]; then
    echo "Filter is empty. Default will be used."
fi

indicator=$4
if [ "$filepath" == "" ]; then
    echo "Indicator is empty. Default will be used."
fi
echo "Calculating..."
echo ""
php index.php statistics:payments $filepath $format $filter $indicator