<?php
/**
 * @author Eugene Glotov <kivagant@gmail.com>
 * @package test
 */

return [
    'statistics' => [
        'payments' => [
            'views' => [
                'available' => [
                    'plain_text' => KIVagant\StatementParser\Views\PlainTextView::class,
                    'json' => KIVagant\StatementParser\Views\JsonView::class,
                ],
                'default' => 'plain_text',
            ],
            'filters' => [
                'available' => [
                    'all' => KIVagant\StatementParser\Statistics\Filters\AllFilter::class,
                    'payments_reference' => KIVagant\StatementParser\Statistics\Filters\PaymentReferenceFilter::class,
                    'cursol' => KIVagant\StatementParser\Statistics\Filters\CursolFilter::class,
                ],
                'default' => 'payments_reference',
            ],
            'indicators' => [
                'available' => [
                    'sum_by_currency' => KIVagant\StatementParser\Statistics\Indicators\SumByCurrencyIndicator::class,
                ],
                'default' => 'sum_by_currency',
            ],
        ],
    ],
];