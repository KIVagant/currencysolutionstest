<?php
/**
 * @author Eugene Glotov <kivagant@gmail.com>
 * @package test
 */

return [

    // Command that will be executed by default
    'default_command' => 'statistics:payments',

    // Each command should be registered as service, see services.php
    'commands' => [
        'statistics:payments',
    ],

    'resources' => [

        // Default resource file with bank report
        'default' => 'resources/statement.csv',
    ],
];