<?php
/**
 * @author Eugene Glotov <kivagant@gmail.com>
 * @package test
 */

return [
    'services' => [
        'statistics:payments' => [
            'class' => \KIVagant\StatementParser\Commands\Statistics\PaymentsCommand::class,
            'arguments' => [
                '',
                '@config',
            ],
        ],
    ],
];